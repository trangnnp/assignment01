package model

import (
	"time"
	"math/rand"
	"log"
	"fmt"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"bitbucket.org/trang/assignment01/db"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Dataa struct {
    FullLink  string        `bson:"fulllink"`
	ShortenLink string		`bson:"shortenlink"`
	Used int				`bson:"used"`
	CreatedDate time.Time	`bson:"createddate"`
	UpdateDate time.Time	`bson:"updatedate"`
}

func Add(full string, short string) Dataa {
	collec:=db.GetCollection("maindatas")
	if (short == "") {
		short = "Trang."+RandStringBytes(9)	
	}
	data := Dataa {
		FullLink: full,
		ShortenLink: short,
		Used: 0,	
		CreatedDate: time.Now(),
		UpdateDate: time.Now() }
	insertResult, errr := collec.InsertOne(context.TODO(), &data)
	if (errr!=nil){
		log.Fatal(errr)
		fmt.Println(insertResult)
	}	
	fmt.Println(data)
	return data
}

func GetOne(alias string) Dataa{
	collec:=db.GetCollection("maindatas")
	filter:=bson.M{ "$or": []bson.M{ bson.M{"fulllink":alias}, bson.M{"shortenlink": alias} } } 
	var p Dataa
	if err := collec.FindOne(context.TODO(), filter).Decode(&p); err != nil {
		log.Fatal(err)
	}
	p.Used+=1
	update := bson.M{"$set": bson.M{"updatedate": time.Now(), "used": p.Used}}
	res, err := collec.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
		fmt.Println(res.ModifiedCount)
	}
	return p
}

func GetAll(offset int, limit int) ([]Dataa){
	collec:=db.GetCollection("maindatas")
	filter := bson.M{}
	var p1 []Dataa
	options := options.FindOptions{}
	var tmp=int64(limit)
	options.Limit = &tmp
	options.SetSkip(int64(int64(limit)*(int64(offset)-1)))
	cursor, err := collec.Find(context.Background(), filter, &options)
	if err != nil {
		log.Fatal(err)
	}
	for cursor.Next(context.Background()) {
		var p Dataa
		// decode the document
		if err := cursor.Decode(&p); err != nil {
			log.Fatal(err)
		}
		p1=append(p1,p)
	}
	return p1
}

func Update(alias string, new Dataa	) int64{
	collec:=db.GetCollection("maindatas")
	update := bson.M{"$set": bson.M{"updatedate": time.Now()}}
	filter := bson.M{"fulllink": alias}
	res, err := collec.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	return res.ModifiedCount
}

func UpdateUsed(alias string, new Dataa	) int64{
	collec:=db.GetCollection("maindatas")
	update := bson.M{"$set": bson.M{"updatedate": time.Now()}}
	filter := bson.M{"fulllink": alias}
	res, err := collec.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	return res.ModifiedCount
}

func Remove(alias string) int64{
	collec:=db.GetCollection("maindatas")
	filter:=bson.M{ "$or": []bson.M{ bson.M{"fulllink":alias}, bson.M{"shortenlink": alias} } } 
	res, err := collec.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}
	return res.DeletedCount	
}

func RandStringBytes(n int) string {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    b := make([]byte, n)
    for i := range b {
        b[i] = letterBytes[rand.Intn(len(letterBytes))]
    }
    return string(b)
}