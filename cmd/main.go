package main

import (
	"bitbucket.org/trang/assignment01/db"
	"bitbucket.org/trang/assignment01/router"
)

func main(){
	db.DB_connect()
	router.Routerdef()
}