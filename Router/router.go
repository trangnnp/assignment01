package router

import (
    "github.com/go-chi/chi"
    "Controller"
    "net/http"
)

func Routerdef(){
    routerx := chi.NewRouter()
    routerx.Post("/data", user.InputData)
    routerx.Get("/data/{alias}", user.GetOne)
    routerx.Get("/data", user.GetAll)
    routerx.Put("/data/{alias}", user.Update)
    routerx.Delete("/data/{alias}", user.Remove)
    http.ListenAndServe(":8080", routerx)
}
